#!/bin/bash
# Compares the first 1000 lines of some results file against truth.top
#
# Usage:
#   truth_eval.sh input1.top input2.top ...

truth="$(git rev-parse --show-toplevel)/data/truth.top"

function nlines() {
    wc -l "$1" | cut -d' ' -f1
}

function min() {
    echo $(($1 < $2 ? $1 : $2))
}

function truth_compare() {
    ninput=$(nlines "$1")
    ntruth=$(nlines "$truth")
    nmin=$(min $ninput $ntruth)
    diff -y --suppress-common-lines <(head -$nmin $1) <(head -$nmin $truth)
}

for input in $@; do
    diffs="$(truth_compare $input)"
    if [[ $? -eq 0 ]]; then
        echo "$input: full match"
    else
        nerrors=$(echo "$diffs" | wc -l)
        echo "$input: $nerrors not matching"
        truth_compare $input
    fi
done
