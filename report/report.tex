\documentclass[a4paper,twocolumn,twoside]{article}

\usepackage{hyperref}
\usepackage{xspace}
\usepackage{titling}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage[ruled,vlined]{algorithm2e}
\usepackage[margin=0.60in]{geometry}

\newcommand*{\eg}{e.g.\@\xspace}
\newcommand*{\lbr}{\ensuremath{\left\{}}
\newcommand*{\rbr}{\ensuremath{\right\}}}
\newcommand*{\st}{\ensuremath{~|~}}
\newcommand*{\norm}[1]{\ensuremath{\left|\left|#1\right|\right|}}
\newcommand*{\textapprox}{\raisebox{0.5ex}{\texttildelow}}

\setlength{\droptitle}{-4em}
\date{\vspace{-2em}}
\pagenumbering{gobble}

% Write a report outlining the decisions you made in implementing the search
% algorithms, as well as your comments on the relative performance of the
% techniques you have tried.  Provide specific details on the methods and their
% parameters.  Include formulas, tables and graphs if necessary.  You are
% allowed a maximum of 2 pages for the report, but shorter reports are fine.
% Submit a paper copy of the report to the ITO.

\title{Text Technologies - Assessment 2}
\author{Clemens Wolff (s0942284)}

\begin{document}
\maketitle

\section{Introduction}\label{sec:introduction}

This report investigates how different query execution strategies affect the
speed of a search engine processing conjunctive queries on a collection of
tweets. Section~\ref{sec:algorithms} evaluates the performance of a number of
standard query execution strategies.  Section~\ref{sec:optimization} explores
heuristics to improve execution speed.  Section~\ref{sec:conclusion} synthesises
the findings to propose a fast algorithm for the problem domain and concludes
the report.

\section{Basic query execution strategies}\label{sec:algorithms}

Implementing the ``brute-force'' algorithm proposed in \cite{tts-assign2} gives
us a performance baseline: a na\"{i}ve approach to our benchmark (retrieving the
five newest tweets out of a collection of 22,310 tweets for 4,747 queries) takes
about 97 seconds.

Next, we implement the conjunctive versions of the ``document at a time'' (DAAT)
and ``term at a time'' (TAAT) strategies presented in
\cite[Fig.~5.20,~Fig.~5.21]{ir-book} and summarized in pseudo-code in
Algorithm~\ref{algo:daat} and Algorithm~\ref{algo:taat}. DAAT and TAAT employ an
inverted index\footnote{Our implementation follows that of
\cite[p.~37]{tts-indexing}.} of words to documents to reduce the search space
during query evaluation by only considering documents that could potentially be
matches for the query.  Noteworthy implementation details are that DAAT
eliminates documents that do not contain all of the query terms during the
``merge'' step, and that TAAT is implemented using sets due to their promise of
(amortized) constant-time membership checks \cite{python-performance-docs}.

Generally speaking, TAAT trades off better time complexity for DAAT's lower
memory requirements \cite[p.~29]{tts-indexing}. This holds true for our
data-set: DAAT uses about 7\% less memory\footnote{This number was obtained
using Python's {\tt resource} module and should therefore be considered no more
than a very rough estimate.} but takes 22 seconds to run where TAAT only needs
about 5 seconds. Note that TAAT is very fast because we are only processing
conjunctive queries. This means that we can maintain a set of candidate
documents that could match the current query and reduce that set as more query
terms are processed by computing the set intersection between the candidates and
the entries in the inverted index of the new query term. This is fast for two
reasons: computing the set intersection is cheap\footnote{We can compute the
intersection of two sets $A$ and $B$ in $O(min(\norm{A}, \norm{B}))$ time if the
set membership test is $O(1)$.} and it saves us having to iterate through the
list of partial scores that standard TAAT requires. Note that TAAT also has an
edge over DAAT because our data-set fits in memory so that DAAT's space
complexity advantage is less beneficial.

\begin{algorithm}[h]
    \KwIn{$Documents$, (document id, words) tuples}
    \KwIn{$Queries$, (query id, words) tuples}
    \hspace{1em}\\
    $I \gets InvertedIndex(Documents)$\\
    \ForEach{$(Q_{id}, Q) \in Queries$}{%
        $L \gets \lbr I_q \st q \in Q\rbr$\\
        $R \gets L.pop()$\\
        \ForEach{$l \in L$}{%
            initialize $r$ as an array\\
            $P_R, P_l$ point to $R,~l$\\
            \While{$P_R < \norm{R}$ and $P_l < \norm{l}$}{%
                $x_R, x_l \gets R_{P_R}, l_{P_l}$\\
                \If{$x_R = x_l$}{%
                    add $x_R$ to $r$\\
                    increment both pointers
                }\Else{%
                    increment the pointer to $min(x_R,x_l)$
                }
            }
            $R \gets r$\\
        }
        Print the five highest document-ids in $R$
    }
    \caption{Document at a time}
    \label{algo:daat}
\end{algorithm}

\begin{algorithm}[h]
    \KwIn{$Documents$, (document id, words) tuples}
    \KwIn{$Queries$, (query id, words) tuples}
    \hspace{1em}\\
    $I \gets InvertedIndex(Documents)$\\
    \ForEach{$(Q_{id}, Q) \in Queries$}{%
        $R \gets Set(I_{Q.pop()})$\\
        \ForEach{$q \in Q$}{%
            $R \gets R \cap I_q$\\
        }
        Print the five highest document-ids in $R$
    }
    \caption{Term at a time}
    \label{algo:taat}
\end{algorithm}

\section{Optimizing query execution}\label{sec:optimization}

The ``merge'' routine that is at the heart of DAAT strategies can be a common
source of inefficiency, especially when we are processing inverted index entries
of very different lengths. Merging two lists of sizes $m$ and $n$ takes $O(m +
n)$ time even though we might have $m << n$. One way to address this is the
``MaxScore'' method \cite[p.~177-178]{ir-book} which in the case of conjunctive
queries reduces to iterating over the documents in the shorter list and finding
matching documents in the longer list using binary search
\cite[p.~26]{tts-indexing}.  Implementing this optimization improves execution
time massively: DAAT now only takes 4.9 seconds to complete our benchmark.  This
gain in performance can be explained by the fact that the sizes of lists
processed by ``merge'' differ drastically in our data-set (ratio of length of
shorter list to length of longer list $\mu = 0.12,~\sigma = 0.24$).

Switching between linear merge and binary search dynamically depending on the
difference in lengths of the lists did not improve performance. We suspect that
this is due to the actual execution speed (as opposed to the asymptotic time
complexity) of our search-merge routine being a lot faster than our linear-merge
routine: Python is bad at dealing with pointers and indices\footnote{Some ad-hoc
performance experiments suggest that accessing elements from a Python list using
a {\tt for-each} construct is about two times faster than using index notation.}
and our corpus is not big enough to be able to discount such implementation
details. This assertion is supported by the observation that implementing DAAT
using simultaneous k-way merge which is $O(kn)$ asymptotically instead of the
$O(n^2)$ of iterative incremental pairwise merge previously implemented (with $k
< n$ in general\footnote{$k$ is the number of inverted index entries to be
merged, $n$ is the length of the longest such entry.}) did also not improve
performance. For similar reasons, techniques such as distributing the index and
searching fragments independently did also not speed up the algorithms: the
set-up cost is too high for such a small data-set.

One way to optimize the TAAT query execution strategy is to make use of the fact
that we can process query terms in any order we chose and still get the same
final result. When dealing with conjunctive queries we could, for example,
process the query terms in order of decreasing rarity \cite[p.~174]{ir-book}.
This minimizes the size of our candidate set at any given time and therewith
minimizes the time spent to compute the set intersection between the current
candidate documents and the inverted index entry of the next query term. This
technique is called ``prioritized processing'' and works well on our data-set:
TAAT now only takes 3.8 seconds to complete the benchmark (number of saved
operations when computing set intersection $\mu = 9,344,~\sigma = 10,881$).

Query caching \cite[ch.~5.7.6]{ir-book} was not considered because no queries
were repeated in our training set. Even in a similar but much bigger corpus
(84,983 queries), only about 0.1\% of queries are non-unique which supports our
decision to not explore query caching: maintaining a cache with such a low
hit-rate would be a poor investment in both space and time terms.

Approximate query optimization techniques such as early termination
\cite[p.~178-179]{ir-book} were not considered because this report is only
interested in exact methods; terminating a proritized processing TAAT algorithm
as soon as five candidate documents have been retrieved introduces at least 53
errors for the first 1,000 queries in our data-set.

\section{Conclusion}\label{sec:conclusion}

This report investigated the effect of a variety of factors on the speed of a
search engine processing conjunctive queries on a corpus of tweets.
Section~\ref{sec:algorithms} showed that ``term at a time'' strategies
outperform ``document at a time'' strategies because our data-set is small and
fits in memory. Section~\ref{sec:optimization} proposed a variety of methods to
improve the run-time of both types of approaches, with the former still coming
out ahead.

Synthesising these insights, we propose a search engine based on ``term at a
time'' execution that uses prioritized processing and various Python-specific
optimizations (\eg using an inverted index based on sets instead of lists for
fast contains-check). The system completes our benchmark in under 0.6 seconds
which is a substantial improvement on the standard algorithms proposed in
Section~\ref{sec:algorithms} (Figure~\ref{fig:runtimes}). A \textapprox 250
times bigger benchmark is completed in under three minutes
(Figure~\ref{fig:runtimes-big}).

\begin{figure}[t]
    \centering
    \includegraphics[width=\columnwidth]{runtimes}
    \caption{Run-times of query execution strategies}
    \label{fig:runtimes}
\end{figure}

\begin{figure}[t]
    \centering
    \includegraphics[width=\columnwidth]{runtimes-big}
    \caption{Run-times on a more substantial data-set}
    \label{fig:runtimes-big}
\end{figure}

\begin{thebibliography}{9}

\bibitem{tts-assign2}
    V. Lavrenko and D. Wurzer,
    \emph{Text Technologies --- Assignment 2},
    The University of Edinburgh,
    7 October 2013,
    available on-line at
        \url{http://www.inf.ed.ac.uk/teaching/courses/tts/assessed/assessment2.html}

\bibitem{ir-book}
    B. Croft, D. Metzler and T. Strohman,
    \emph{Search Engines: Information Retrieval In Practice},
    Addison-Wesley Publishing Company,
    2009

\bibitem{python-performance-docs}
    Python documentation,
    \emph{Time Complexity},
    retrieved 19 October 2013 at
        \url{https://wiki.python.org/moin/TimeComplexity}

\bibitem{tts-indexing}
    V. Lavrenko,
    \emph{Text Technologies --- Indexing},
    Lecture at the University of Edinburgh,
    7,10,14 October 2013,
    available on-line at
        \url{http://www.inf.ed.ac.uk/teaching/courses/tts/pdf/index-2x2.pdf}

\end{thebibliography}


\end{document}
