#!/usr/bin/python

from itertools import imap, izip

from irsystem import inverted_index, preprocess, to_str
from util import read_lines, write_lines, binary_search


def search_merge(A, B):
    short_li, long_li = (A, B) if len(A) < len(B) else (B, A)
    for elem in short_li:
        try:
            binary_search(elem, long_li)
            yield elem
        except ValueError:
            pass


def linear_merge(A, B):
    lenA = len(A)
    lenB = len(B)
    ptr_A = 0
    ptr_B = 0
    while ptr_A < lenA and ptr_B < lenB:
        a = A[ptr_A]
        b = B[ptr_B]
        if a == b:
            ptr_A += 1
            ptr_B += 1
            yield a
        elif a < b:
            ptr_A += 1
        else:
            ptr_B += 1


def true_kway_merge(ls):
    def _merge(ls):
        ls = list(ls)
        num_ls = len(ls)
        idx = range(num_ls)
        lens = map(len, ls)
        ptrs = [0] * num_ls

        def get_ptr_elems():
            for i in idx:
                yield ls[i][ptrs[i]]

        def incr_all_ptrs():
            for i in idx:
                ptrs[i] += 1

        def all_match(elem, elems):
            return all(elems[i] == elem for i in idx)

        def ptr_to_smallest_elem(elems):
            return min(izip(elems, idx))[1]

        while all(ptrs[i] < lens[i] for i in idx):
            Xs = list(get_ptr_elems())
            if all_match(Xs[0], Xs):
                incr_all_ptrs()
                yield Xs[0]
            else:
                ptrs[ptr_to_smallest_elem(Xs)] += 1

    return list(_merge(ls))


def simple_kway_merge(ls):
    ls = iter(ls)
    result = next(ls)
    for l in ls:
        result = list(linear_merge(result, l))
    return result


def docs(docs_path, qrys_path, out_path):
    docs = preprocess(read_lines(docs_path))
    qrys = preprocess(read_lines(qrys_path))

    index = inverted_index(docs)

    def process_queries():
        for qry_id, qry in qrys:
            inv_lists = (index[term] for term in qry)
            retrieved = simple_kway_merge(inv_lists)[::-1][:5]
            yield qry_id, retrieved

    result_strs = imap(to_str, process_queries())
    write_lines(out_path, result_strs)


if __name__ == '__main__':
    docs('docs.txt', 'qrys.txt', 'docs.top')
