#!/usr/bin/python


from itertools import imap

from irsystem import preprocess, to_str
from util import nlargest, read_lines, write_lines


def matches(query, document):
    for term in query:
        if term not in document:
            return False
    return True


def brute(docs_path, qrys_path, out_path):
    docs = preprocess(read_lines(docs_path))
    qrys = preprocess(read_lines(qrys_path))

    def process_queries():
        for qry_id, qry in qrys:
            candidates = (doc_id for doc_id, doc in docs if matches(qry, doc))
            retrieved = nlargest(5, candidates)
            yield qry_id, retrieved

    result_strs = imap(to_str, process_queries())
    write_lines(out_path, result_strs)


if __name__ == '__main__':
    brute('docs.txt', 'qrys.txt', 'brute.top')
