#!/usr/bin/python


from itertools import imap

from irsystem import inverted_index, preprocess, to_str
from util import nlargest, read_lines, write_lines


def terms(docs_path, qrys_path, out_path):
    docs = preprocess(read_lines(docs_path))
    qrys = preprocess(read_lines(qrys_path))

    inv_index = inverted_index(docs)

    def process_queries():
        for qry_id, qry in qrys:
            candidates = set(inv_index[qry.pop()])
            for term in qry:
                candidates.intersection_update(inv_index[term])
            retrieved = nlargest(5, candidates)
            yield qry_id, retrieved

    result_strs = imap(to_str, process_queries())
    write_lines(out_path, result_strs)


if __name__ == '__main__':
    terms('docs.txt', 'qrys.txt', 'terms.top')
