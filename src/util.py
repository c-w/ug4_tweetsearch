import bisect
import sys
import time


def binary_search(x, li, lo=0, hi=None):
    hi = len(li) if hi is None else hi
    pos = bisect.bisect_left(li, x, lo, hi)
    if pos != hi and li[pos] == x:
        return pos
    raise ValueError('element not in list')


def nlargest(n, it, reverse=True):
    return sorted(it, reverse=reverse)[:5]


def timeit(fn):
    def timed(*args, **kwargs):
        ti = time.time()
        rval = fn(*args, **kwargs)
        tf = time.time()
        print >> sys.stderr, '%s ---> %gs' % (fn.__name__, tf - ti)
        return rval
    return timed


def read_lines(path):
    with open(path, 'r') as f:
        lines = [l.strip() for l in f]
    return lines


def write_lines(path, lines):
    with open(path, 'w') as f:
        f.write('\n'.join(lines))
