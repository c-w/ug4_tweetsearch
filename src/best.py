#!/usr/bin/python

from collections import defaultdict
from itertools import imap

from irsystem import make_entry, to_str
from util import write_lines


def fast_index(docs):
    index = defaultdict(set)
    for doc_id, doc in docs:
        for term in doc:
            index[term].add(doc_id)
    return index


def best(docs_path, qrys_path, out_path):
    with open(docs_path) as docs_f:
        with open(qrys_path) as qrys_f:
            docs = (make_entry(l) for l in docs_f.xreadlines())
            qrys = [make_entry(l) for l in qrys_f]
            inv_index = fast_index(docs)

            def _key(t):
                return len(inv_index[t])

            def process_queries():
                for qry_id, qry in qrys:
                    qry.sort(key=_key)
                    qry_it = iter(qry)
                    candidates = set(inv_index[next(qry_it)])
                    for term in qry_it:
                        candidates &= inv_index[term]
                    retrieved = sorted(candidates, reverse=True)[:5]
                    yield qry_id, retrieved

            result_strs = imap(to_str, process_queries())
    write_lines(out_path, result_strs)


if __name__ == '__main__':
    best('docs.txt', 'qrys.txt', 'best.top')
