from collections import defaultdict
from string import maketrans, translate, punctuation


def to_str((qry_id, retrieved)):
    return '%d %s' % (qry_id, ' '.join(map(str, retrieved)))


def tokenize(text):
    try:
        transtable = tokenize._transtable
    except AttributeError:
        delims = punctuation
        transtable = maketrans(delims, ' ' * len(delims))
        tokenize._transtable = transtable

    return [w.lower() for w in translate(text, transtable).split()]


def make_entry(line):
    parts = line.split(' ', 1)
    ident = int(parts[0])
    try:
        return ident, tokenize(parts[1])
    except IndexError:
        return ident, ''


def preprocess(lines, store=None):
    return ([make_entry(l) for l in lines] if store is None
            else store(make_entry(l) for l in lines))


def _append(li, e):
    li.append(e)


def inverted_index(docs, store=list, adder=_append, sort=True):
    index = defaultdict(store)
    for doc_id, doc in docs:
        for term in doc:
            adder(index[term], doc_id)

    if sort:
        for term, postings in index.iteritems():
            index[term] = sorted(set(postings))
    return index
