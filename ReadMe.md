# Text Technologies Assessment 2

## Dataset

In this coursework, you will develop a search engine for Twitter. Social Network
search is different in two ways from web-search:

- a tweet matches the query only if it contains all query words (i.e. the query
  is a Boolean AND)
- matching tweets are ranked by time (most recent first), and not by their
  similarity to the query

## Tasks

Your goal is to develop the fastest possible search algorithm by exploring a
range of indexing and query-execution methods.

1. Implement a brute-force algorithm for finding the matching documents. Your
   implementation should load the queries from qrys.txt and documents from
   docs.txt in the current directory. Tokenize on punctuation and lowercase the
   tokens, but do not perform any other pre-processing. For each query, print up
   to 5 most recent matches into a file called brute.top in the current
   directory.

2. Implement the term-at-a-time retrieval algorithm. Your implementation should
   load and tokenize the documents and queries exactly as in task 1. Then build
   an inverted index for each word, and run each query against the index using
   the term-at-a-time execution strategy. Do not use any heuristics to speed up
   the algorithm. Print up to 5 most recent matches into a file called terms.top
   in the current directory.

3. Implement the doc-at-a-time retrieval algorithm. Tokenize and index the
   documents as in task 2. Run each query against the index using the
   document-at-a-time execution strategy. Use simple linear merge and do not use
   any heuristics to speed up the algorithm. Print up to 5 most recent matches
   into a file called docs.top in the current directory.

4. Try to improve the speed of your algorithms. You can use any methods and
   heuristics to improve the speed, as long as the output stays the same. Print
   up to 5 most recent matches into best.top in the current directory.

5. Compare running time of your algorithms. Take N=100 first documents and
   measure how many seconds it takes your algorithms from tasks 1, 2 and 3 to
   process all the queries. The time should include all stages of the algorithm
   (loading the data, tokenizing, indexing, query execution, printing of
   results, and anything else you do). Use the following code:
   ``started = time.time() ... runtime = time.time() - started``
   Repeat for N=100,200,500,1000,5000,10000,20000. Average running time over
   several runs and plot the running time against N (use logarithmic scale).

6. Write a report summarizing what you have learned from this assignment. Give
   full details for the methods you used in part 4. You are allowed a maximum of
   2 pages for the report. Include the plot from task 5, and any other
   plots/tables if necessary (plots and tables do not count towards the 2-page
   limit).

Once you match the query to a set of documents, print the results in the
following format:

- 1 1098 1089
- 2 1098 1089
- 3 905 900 894
- 4 14923 14915 14906 14872 14864
- 5 15655 15649 15639

This means that query 4 matched many documents and we are printing the 5 most
recent ones. The other queries matched fewer than 5 documents.

## Data and Formats

### docs.txt

Each document is on a separate line. The first field is a unique document
number, followed by the document content, all on the same line. For example:

- 1 RT @Dohnanyi: Anyone who knows about Swiss nationals caught up in the
  terrorist attacks? pls reply to my e-mail ...
- 2 @mumbai did they ever find the two cars?

Documents are listed in chronological order (last one is the most recent).

### qrys.txt

Each query is given on a separate line. The first field is the query number,
followed by the query itself, as follows:

- 1 taj paintings
- 50 live video stream the fire and fights taj hotel

### truth.top

Correct output for the first 1000 queries.
